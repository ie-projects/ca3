<%@ page import="java.util.ArrayList" %>
<%@ page import="org.fn.ie.PartyRestaurant" %>
<%@ page import="org.fn.ie.PartyFood" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    ArrayList<PartyRestaurant> partyReses = (ArrayList<PartyRestaurant>) request.getAttribute("partyFoodReses");
%>
<html>
<head>
    <title>Food Party!</title>
</head>
<body>
<style>
    img {
        width: 50px;
        height: 50px;
    }
    li {
        display: flex;
        flex-direction: row;
        padding: 0 0 5px;
    }
    div, form {
        padding: 0 5px
    }
</style>
Welcome to food party! We have <%=partyReses.size()%> restaurants to offer discount.
<ul>
    <% for(int j = 0; j < partyReses.size(); j++)
    {%>
    <li>id: <%=partyReses.get(j).getId()%> </li>
    <li>name: <%=partyReses.get(j).getName()%> </li>
    <li>location: ( <%=partyReses.get(j).getLocation().getX()%>, <%=partyReses.get(j).getLocation().getY()%> )</li>
    <li>logo: <img src="<%=partyReses.get(j).getLogo()%>" alt="logo"></li>
    <li>menu:
        <ul>
            <%ArrayList<PartyFood> resMenu = partyReses.get(j).getMenu();
                for (int i = 0; i < resMenu.size(); i++)
                {%>
            <li>
                <img src="<%=resMenu.get(i).getImage()%>" alt=\"logo\">
                <div><%=resMenu.get(i).getName()%></div>
                <div>New Price: <%=resMenu.get(i).getPrice()%> Toman</div>
                <div>Old Price: <%=resMenu.get(i).getOldPrice()%> Toman</div>
                <div>Remaining: <%=resMenu.get(i).getCount()%></div>
                <form action="/add_party_to_cart/<%=partyReses.get(j).getId()%>/<%=resMenu.get(i).getName()%>" method="POST">
                    <input type="hidden" value="<%=resMenu.get(i).getName()%>" name="foodName" />
                    <button type="submit">addToCart</button>
                </form>
            </li>
            <%}%>
        </ul>
    </li>
    <%}%>
</ul>
</body>
</html>
