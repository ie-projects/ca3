<%@ page import="java.util.ArrayList" %>
<%@ page import="org.fn.ie.Restaurant" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%
    ArrayList<Restaurant> restaurants = (ArrayList<Restaurant>) request.getAttribute("restaurants");
%>
<html>
<head>
    <meta charset=\"UTF-8\">
    <title>Restaurants</title>
    <style>
        table {
            text-align: center;
            margin: auto;
        }
        th, td {
            padding: 5px;
            text-align: center;
        }
        .logo{
            width: 100px;
            height: 100px;
        }
    </style>
</head>
<body>
    <table>
            <tr>
                    <th>id</th>
                    <th>logo</th>
                    <th>name</th>
                    <th>Estimated delivery time</th>
                </tr>
        <%
        for(int i = 0; i < restaurants.size(); i++)
        {%>
        <tr>
                        <td><%=restaurants.get(i).getId()%></td>
                        <td><img class="logo" src="<%=restaurants.get(i).getLogo()%>" alt="logo"></td>
                        <td><%=restaurants.get(i).getName()%></td>
                        <td><%=60 + ((int)(1.5 * Math.hypot(restaurants.get(i).getLocation().getX(),
                            restaurants.get(i).getLocation().getY()) / 5)) %> seconds</td>
        </tr>
        <%}%>

        </table>
</body>
</html>