<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    boolean readable = (boolean) request.getAttribute("readable");
%>

<html>
<head>
    <title>Credit Increase</title>
</head>
<body>
    <% if(readable)
    {%>
    <div>Transaction completed!</div>
    <%}
    else
    {%>
    <div>Invalid Format!</div>
    <%}%>

</body>
</html>
