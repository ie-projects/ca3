package org.fn.ie.controllers;

import org.fn.ie.Nikivery;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/food_party")
public class food_party extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("partyFoodReses", Nikivery.getInstance().getPartyFoodReses());
        String pageName = "/food_party.jsp";
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(pageName);
        requestDispatcher.forward(request, response);
    }
}
