package org.fn.ie.controllers;

import org.fn.ie.Nikivery;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/order_status/*")
public class order_status extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathInfo = request.getRequestURI();
        String[] parts = pathInfo.split("/");
        String id = parts[2];
        ArrayList<String> stats = Nikivery.getInstance().getUserProfile().getDeliveryState();
        boolean isValid = false;
        try {
            isValid = stats.size() > Integer.parseInt(id);
        }
        catch (NumberFormatException e)
        {
            isValid = false;
        }
        request.setAttribute("isValid", isValid);
        if(isValid)
            request.setAttribute("status", stats.get(Integer.parseInt(id)));
        String pageName = "/order_status.jsp";
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(pageName);
        requestDispatcher.forward(request, response);
    }
}
