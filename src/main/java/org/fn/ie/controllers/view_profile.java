package org.fn.ie.controllers;

import org.fn.ie.Nikivery;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/view_profile")
public class view_profile extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("user", Nikivery.getInstance().getUserProfile());
        String pageName = "view_profile.jsp";
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(pageName);
        requestDispatcher.forward(request, response);
    }
}
