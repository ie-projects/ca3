package org.fn.ie.controllers;

import org.fn.ie.Nikivery;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/credit_up")
public class credit_up extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String up = request.getParameter("credit");
        boolean readable = false;
        try {
            Nikivery.getInstance().getUserProfile().addCredit(Integer.parseInt(up));
            readable = true;
        }
        catch (NumberFormatException e)
        {
        }
        finally {
            request.setAttribute("readable", readable);
            String pageName = "credit_up.jsp";
            RequestDispatcher requestDispatcher = request.getRequestDispatcher(pageName);
            requestDispatcher.forward(request, response);
        }
    }

}
