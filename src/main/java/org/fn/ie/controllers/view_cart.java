package org.fn.ie.controllers;

import org.fn.ie.Food;
import org.fn.ie.Nikivery;
import org.fn.ie.Order;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/view_cart")
public class view_cart extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<Order> cart = Nikivery.getInstance().getUserProfile().getCart();
        boolean isCartEmpty;
        isCartEmpty = (cart.size() == 0);
        request.setAttribute("isCartEmpty", isCartEmpty);
        request.setAttribute("cartRestaurant", Nikivery.getInstance().getUserProfile().getCartRestaurant());
        request.setAttribute("cart", cart);
        String pageName = "view_cart.jsp";
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(pageName);
        requestDispatcher.forward(request, response);
    }
}
