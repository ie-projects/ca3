package org.fn.ie.controllers;

import org.fn.ie.Nikivery;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@WebServlet("/add_party_to_cart/*")
public class add_party_to_cart extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String foodName = request.getParameter("foodName");
        byte[] bytes = foodName.getBytes(StandardCharsets.ISO_8859_1);
        foodName = new String(bytes, StandardCharsets.UTF_8);

        String pathInfo = request.getRequestURI();
        String[] parts = pathInfo.split("/");
        String resId = parts[2];

        int status = Nikivery.getInstance().addToCart(foodName, resId, true);
        request.setAttribute("status", status);
        String pageName = "/add_to_cart.jsp";
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(pageName);
        requestDispatcher.forward(request, response);
    }
}
