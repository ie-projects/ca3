package org.fn.ie.controllers;

import org.fn.ie.Food;
import org.fn.ie.Nikivery;
import org.fn.ie.Restaurant;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/restaurants/*")
public class restaurant extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathInfo = request.getRequestURI();
        String[] parts = pathInfo.split("/");
        String id = parts[2];
        int status = 0;
        Restaurant res = Nikivery.getInstance().getRestaurant(id);
        if(res == null)
        {
            status = -1;
        }
        else if(Math.hypot(res.getLocation().getX(), res.getLocation().getY()) > 170)
        {
            status = -2;
        }
        else {
            request.setAttribute("res", res);
        }
        request.setAttribute("status", status);
        String pageName = "/restaurant.jsp";
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(pageName);
        requestDispatcher.forward(request, response);
    }
}
