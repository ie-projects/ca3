package org.fn.ie;

import java.util.ArrayList;

public class UserProfile {
    private int id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private int credit;
    private ArrayList<Food> cart;
    private String cartRestaurant;
    private ArrayList<String> deliveryStates;

    public UserProfile(int _id, String fname, String lname, String pnumber, String mail, int cred)
    {
        id = _id;
        firstName = fname;
        lastName = lname;
        phoneNumber = pnumber;
        email = mail;
        credit = cred;
        cart = new ArrayList<Food>();
        deliveryStates = new ArrayList<String>();
    }

    public int getId() { return id; }
    public String getFirstName() { return firstName; }
    public String getLastName() { return lastName; }
    public String getPhoneNumber() { return phoneNumber; }
    public String getEmail() { return email; }
    public int getCredit() { return credit; }
    public String getCartRestaurant() { return cartRestaurant; }
    public void addCredit(int up) { credit += up; }
    public void addFoodToCart(Food food) { cart.add(food); }
    public void setCartRestaurant(String cartRestaurant) { this.cartRestaurant = cartRestaurant; }
    public void setDeliveryState(String deliveryState, int i)
    {
        if(i == -1)
            deliveryStates.add(deliveryState);
        else if(i < deliveryStates.size())
            deliveryStates.set(i, deliveryState);
    }
    public ArrayList<String> getDeliveryState() { return deliveryStates; }

    private ArrayList<Order> organizeOrders()
    {
        ArrayList<Order> orders = new ArrayList<Order>();
        for(int i = 0; i < cart.size(); i++)
        {
            int foodCount = 1;
            boolean repetitive = false;
            for(int j =0; j < cart.size(); j++)
                if(cart.get(i).getName().equals(cart.get(j).getName()) && cart.get(i).getPrice() == cart.get(j).getPrice())
                {
                    if(i == j)
                        continue;
                    if(j < i)
                    {
                        repetitive = true;
                        break;
                    }
                    foodCount++;
                }
            if(!repetitive)
                orders.add(new Order(cart.get(i).getName(), foodCount, cart.get(i).getPrice()));
        }
        return orders;
    }
    public ArrayList<Order> getCart()
    {
        ArrayList<Order> orders = organizeOrders();
        return orders;
    }

    public void emptyCart()
    {
        cart = new ArrayList<Food>();
    }
}
