<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    int status = (int) request.getAttribute("status");
%>
<html>
<head>
    <title>Finalizing!</title>
</head>
<body>
<% if(status == -1)
{%>
Your cart is empty!
<%}
else if (status == -2)
{%>
You don't have enough credit!
<%}
else if (status == -3)
{%>
Insufficient food left in FoodParty!
<%}
else
{%>
Finalized orders succesfully!
<%}%>
</body>
</html>
