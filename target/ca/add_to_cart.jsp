<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%
    int status = (int) request.getAttribute("status");
%>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add to Cart Status</title>
</head>
<body>
<%  if (status == -1)
    {%>
<div>You currently have food from other restaurants in your cart!</div>
    <%}
    else if (status == -2)
    {%>
<div>Facing troubles finding the food!</div>
    <%}
    else if (status == -3)
    {%>
<div>No more of this food left in party!</div>
    <%}
    else
    {%>
<div>Food added to cart successfully!</div>
    <%}%>