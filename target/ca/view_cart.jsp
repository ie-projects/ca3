<%@ page import="java.util.ArrayList" %>
<%@ page import="org.fn.ie.Order" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%
    boolean isCartEmpty = (boolean) request.getAttribute("isCartEmpty");
    ArrayList<Order> cart = (ArrayList<Order>) request.getAttribute("cart");
    String cartRestaurant = (String) request.getAttribute("cartRestaurant");
%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User</title>
    <style>
        li, div, form {
            padding: 5px
        }
    </style>
</head>
<body>
<% if(isCartEmpty)
   {%>
        <div>You currently have no food in cart!</div>
   <%}
   else
    {%>
<div><%=cartRestaurant%></div>
<ul>
    <%
    for(int i = 0; i < cart.size(); i++)
    {%>
        <li><%=cart.get(i).getFoodName()%>:‌ <%=cart.get(i).getCount()%></li>
    <%}%>
</ul>
<form action="/finalize" method="POST">
    <button type="submit">finalize</button>
</form>
    <%}%>
</body>
</html>