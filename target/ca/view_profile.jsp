<%@ page import="org.fn.ie.UserProfile" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%
    UserProfile user = (UserProfile) request.getAttribute("user");
%>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>User</title>
    <style>
        li {
            padding: 5px
        }
    </style>
</head>

<body>
<ul>
    <li>id: <%=user.getId()%></li>
    <li>full name: <%=user.getFirstName()%> <%=user.getLastName()%></li>
    <li>phone number: <%=user.getPhoneNumber()%></li>
    <li>email: <%=user.getEmail()%>></li>
    <li>credit: <%=user.getCredit()%> Toman</li>
    <form action="/credit_up" method="POST">
        <button type="submit">increase</button>
        <input type="text" name="credit" value="" />
    </form>
    <li>
        Orders :
        <ul>
            <%
            for(int i = 0; i < user.getDeliveryState().size(); i++)
            {%>
            <li>
                <a href="/order_status/<%=i%>">order id: <%=(i + 1)%></a>
            </li>
            <%}%>
        </ul>
    </li>
</ul>
</body>

</html>