<%@ page import="org.fn.ie.Restaurant" %>
<%@ page import="org.fn.ie.Food" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%
    Restaurant res = null;
    int status = (int) request.getAttribute("status");
    if(status == 0) {
        res = (Restaurant) request.getAttribute("res");
    }
%>
<html>
<head>
    <meta charset="UTF-8">
    <title>Restaurant Information</title>
</head>
<body>
<style>
    img {
        width: 50px;
        height: 50px;
    }
    li {
        display: flex;
        flex-direction: row;
        padding: 0 0 5px;
    }
    div, form {
        padding: 0 5px
    }
</style>
<% if (status == -1)
{%>
<div>404 Restaurant not found.</div>
<%} else if (status == -2)
{%>
<div>403 Restaurant not in range.</div>
<%} else
{%>
    <ul>
    <li>id: <%=res.getId()%> </li>
            <li>name: <%=res.getName()%> </li>
            <li>location: ( <%=res.getLocation().getX()%>, <%=res.getLocation().getY()%> )</li>
            <li>logo: <img src="<%=res.getLogo()%>" alt="logo"></li>
            <li>menu:
                <ul>
            <%ArrayList<Food> resMenu = res.getMenu();
                for (int i = 0; i < resMenu.size(); i++)
                {%>
                            <li>
                                        <img src="<%=resMenu.get(i).getImage()%>" alt=\"logo\">
                                        <div><%=resMenu.get(i).getName()%></div>
                                        <div><%=resMenu.get(i).getPrice()%> Toman</div>
                                        <form action="/add_to_cart/<%=res.getId()%>" method="POST">
                                            <input type="hidden" value="<%=resMenu.get(i).getName()%>" name="foodName" />
                                            <button type="submit">addToCart</button>
                                        </form>
                                    </li>
                <%}%>
                    </ul>
                </li>
        </ul>
<%}%>
</body>
</html>
